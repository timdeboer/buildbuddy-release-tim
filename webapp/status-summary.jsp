<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.buildbuddy.locator.BuildBuddyLocator"%>
<%@page import="java.util.Collection"%>
<%@page import="org.buildbuddy.monitor.service.BuildMonitorService"%>
<%@page import="org.buildbuddy.webui.BBWebUtils"%>
<%@page import="org.buildbuddy.monitor.service.BuildTargetInfo"%>

<%final int MAX_LENGTH=50;%>

<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>BuildBuddy Status2</title> 

	<link rel="stylesheet" href="assets/core.css">
	<link rel="stylesheet" href="assets/form-basic.css"> 

</head>

<body>
	<header>
		<span class="bb-green">build</span><span class="bb-red">buddy</span>
  </header>

    <ul>
        <li><a href="status-summary.jsp" class="active">Status</a></li>
        <li><a href="buildbuddy-configuration.jsp" >BuildBuddy Configuration</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" method="post" action="/BuildBuddyController.do">

        	<h1>BuildBuddy Status</h1>
		<%
		BuildMonitorService service = BuildBuddyLocator.getBuildMonitorService(); 
		Collection<BuildTargetInfo> infoList = service.getBuildTargetInformation();
		String message = (String)request.getAttribute(BBWebUtils.STATUS_SUMMARY_MESSAGE_ATTRIBUTE_NAME);
		%>
			<div class="form-basic">
				<div class="main-content">
			<%if(message!=null){%>
				<div class="form-row">
					<div class="goodTextMessage">
						<p><%=StringUtils.replace(StringEscapeUtils.escapeHtml4(message),"\n","<BR>")%></p>
					</div>
				</div>
			<%}%>
				<div class="Table">
				    <div class="Heading">
				        <div class="Cell">
				            <p>Build Target</p>
				        </div>
				        <div class="Cell">
				            <p>Status</p>
				        </div>
				    </div>
				
			<%for(BuildTargetInfo info : infoList){ %>
				    <div class="Row">
					<%if(info.getName().length()>MAX_LENGTH){ %>
				        <div class="Cell">
				            <a target="_self" href="<%=info.getUrl()%>" title="<%=StringUtils.left(info.getName(), MAX_LENGTH-3) + "..." %>"><span title="<%=info.getName()%>"><%=StringUtils.left(info.getName(), MAX_LENGTH-3) + "..." %></span></a>
				        </div>
					<%}else{%>
				        <div class="Cell">
				            <a href="<%=info.getUrl()%>"><%=info.getName()%></a>
				        </div>
				    <%}%>
					<%if(info.getName().length()>21){ %>
				        <div class="Cell">
				            <p><%=info.getBuildState()%></p>
				        </div>
					<%}%>      
				    </div>		
			<%}%>      
					</div>
				</div>      
			</div>
        </form>

    </div>

</body>

</html>
