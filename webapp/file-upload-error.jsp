<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.buildbuddy.monitor.plugins.ValidationFailure"%>
<%@page import="java.util.List"%>
<%@page import="org.buildbuddy.webui.UploadServlet"%>
<%@page import="org.buildbuddy.monitor.plugins.exceptions.ConfigurationException"%>
<%@page import="org.buildbuddy.locator.BuildBuddyLocator"%>
<%@page import="java.util.Collection"%>
<%@page import="org.buildbuddy.monitor.service.BuildMonitorService"%>
<%@page import="org.buildbuddy.webui.BBWebUtils"%>
<%@page import="org.buildbuddy.monitor.service.BuildTargetInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Upload Failed</title> 

	<link rel="stylesheet" href="assets/core.css">
	<link rel="stylesheet" href="assets/form-basic.css"> 

</head>


<body>
	<header>
		<span class="bb-green">build</span><span class="bb-red">buddy</span>
  </header>

    <ul>
        <li><a href="status-summary.jsp">Status</a></li>
       <li><a href="buildbuddy-configuration.jsp">BuildBuddy Configuration</a></li>
    </ul>


		<%
		ConfigurationException exception = (ConfigurationException)request.getAttribute(UploadServlet.CONFIGURATION_EXCEPTION_ATTRIBUTE_KEY);
		List<ValidationFailure> failures = exception.getValidationMessages();
		
		%>
		<div class="form-basic"">
			<div class="main-content">
	            <div class="form-title-row">
	                <h1>Upload Failed</h1>
	            </div>
				<div class="form-row">
					<div class="badTextMessage">
						<p><%=StringUtils.replace(StringEscapeUtils.escapeHtml4(exception.getMessage()),"\n","<BR>")%></p>
					</div>
				</div>
			<%
			if(failures.size()>0){ 
			%>
				<div class="form-row">
					<div class="Table">
					    <div class="Heading">
					        <div class="Cell">
					            <p>Plugin</p>
					        </div>
					        <div class="Cell">
					            <p>Property</p>
					        </div>
					        <div class="Cell">
					            <p>Message</p>
					        </div>
					    </div>
				<%
				for(ValidationFailure failure : failures){ 
				%>
						<div class="Row">
						        <div class="Cell">
						            <p><label><%=StringEscapeUtils.escapeHtml4(failure.getConfigBeanName())%></label></p>
						        </div>
						        <div class="Cell">
						            <p><label"><%=StringEscapeUtils.escapeHtml4(failure.getPropertyName())%></label></p>
						        </div>
						        <div class="Cell">
						            <p><label"><%=StringEscapeUtils.escapeHtml4(failure.getMessage())%></label></p>
						        </div>
						</div>
				<%
				}
			}
			%>
					</div>
					<div class="form-button-row">
					    <ul>
					       <li><a href="buildbuddy-configuration.jsp">Try Again</a></li>
					    </ul>
					</div>
				</div>
			</div>
		</div>
</body>
</html>