<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>BuildBuddy Configuration</title> 

	<link rel="stylesheet" href="assets/core.css">
	<link rel="stylesheet" href="assets/form-basic.css"> 

</head>


<body>
	<header>
		<span class="bb-green">build</span><span class="bb-red">buddy</span>
  </header>

    <ul>
        <li><a href="status-summary.jsp">Status</a></li>
        <li><a href="buildbuddy-configuration.jsp" class="active">BuildBuddy Configuration</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" method="post"  enctype="multipart/form-data" action="UploadServlet.do">

            <div class="form-title-row">
                <h1>Upload New Configuration</h1>
            </div>

			<input type="hidden" name="forwardTo" value="buildbuddy-configuration.jsp"/>
            <div class="form-row">
                <label>
					<input type="file" name="dataFile" id="fileChooser"/>
                </label>
								
            </div>

            <div class="form-button-row">
                <button type="submit">Upload</button>
            </div>

        </form>

    </div>

</body>

</html>
