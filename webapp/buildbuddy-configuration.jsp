<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>BuildBuddy Configuration</title> 

	<link rel="stylesheet" href="assets/core.css">
	<link rel="stylesheet" href="assets/form-basic.css"> 

</head>


<body>
	<header>
		<span class="bb-green">build</span><span class="bb-red">buddy</span>
  </header>

    <ul>
        <li><a href="status-summary.jsp">Status</a></li>
        <li><a href="buildbuddy-configuration.jsp" class="active">BuildBuddy Configuration</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" method="post" action="#">

            <div class="form-title-row">
                <h1>BuildBuddy Configuration</h1>
            </div>

            <div class="form-button-row">
			    <a class="config-button" href="CurrentConfigurationFileDownloadServlet.do">Download Current Config</a>
            </div>
            <div class="form-button-row">
				<a class="config-button" href="buildbuddy-configuration-upload.jsp">Upload New Config</a>
            </div>
            <div class="form-button-row">
				<a class="config-button" href="ExampleConfigurationFileDownloadServlet.do">Download Example Config</a>
            </div>

        </form>

    </div>

</body>

</html>
