#########################################################################################
#Script to be called from cron that starts/restarts buildbuddy if it stops for any reason
#
#########################################################################################
#!/bin/sh
cd `dirname $0`
SCRIPTDIR=`pwd`
cd - > /dev/null 2>&1

if ps -ef | grep -v grep | grep buildbuddy.sh ; then
        exit 0
else
				echo "====================================" >> buildbuddy.log
				echo "starting the budster" 								>> buildbuddy.log
				echo "====================================" >> buildbuddy.log
				$SCRIPTDIR/buildbuddy.sh >> buildbuddy.log 2>&1 &
        exit 0
fi